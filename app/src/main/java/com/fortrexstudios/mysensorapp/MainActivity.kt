package com.fortrexstudios.mysensorapp

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.se.omapi.Session
import android.util.Log
import android.view.View
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), SensorEventListener  {
   private lateinit var sensorManager: SensorManager

    private lateinit var tempSensor: Sensor
    private lateinit var lightSensor: Sensor



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        tempSensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE)
        lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT)


    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
        //TODO("Not yet implemented")
    }

    override fun onSensorChanged(event: SensorEvent?) {
        if (event != null) {
            if (event.sensor == this.tempSensor){
                textViewMain.append("Temperatur: " + (event.values[0]).toString() + " °C" + "\n")
                Log.i("TemperaturSensor", (event.values[0]).toString())
            }
            else if(event.sensor == this.lightSensor){
                textViewMain.append("LightSensor: " + (event.values[0]).toString() + " lux" + "\n")
                Log.i("LightSensor", (event.values[0]).toString())
            }
            scrollViewMain.fullScroll(View.FOCUS_DOWN)
        }

    }

    override fun onResume() {
        super.onResume()
        lightSensor?.also { light ->
            sensorManager.registerListener(this, light, 1000000)
        }
        tempSensor?.also { temperature ->
            sensorManager.registerListener(this, temperature, 1000000)
        }

    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
    }


}